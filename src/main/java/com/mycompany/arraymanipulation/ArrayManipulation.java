/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.arraymanipulation;

/**
 *
 * @author User
 */
public class ArrayManipulation {
    public static void main(String[] args) {
        int[]numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        
        for (int i = 0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }
        
        System.out.println(" ");
        for (int i = 0; i < names.length; i++){
            System.out.println(names[i]);
        }
        
        double[]newDoubles = {1.1,1.2,1.3,1.4};
        for (int i = 0; i < values.length; i++){
            values[i] = newDoubles[i];
            System.out.println(names[i]+ " ");
        }System.out.println(" ");
        
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        System.out.println("Sum of numbers: " + sum);
        
        double max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("Maximum value in values: " + max);
        
         String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        for (String name : reversedNames) {
            System.out.println(name);
        }
        
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - 1 - i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }

        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
